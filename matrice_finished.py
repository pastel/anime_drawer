#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tkinter as tk
from tkinter import filedialog
import serial

arduino = serial.Serial("/dev/ttyUSB0", 9600)


class window(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.createWidgets(master)
        self.grid()

    def createWidgets(self, master):
        self.squares = []
        self.squaresState = []
        self.canvas = tk.Canvas(master, width=400, height=400)
        self.canvas.grid(row=0, column=0, rowspan=3)
        self.canvas.bind("<Button-1>", self.events)
        self.createRectangles()
        
        tk.Button(master, text="Ouvrir", command=self.open).grid(row=0, column=1)
        tk.Button(master,
                  text="Enregistrer",
                  command=self.save).grid(row=1, column=1)
        tk.Button(master, text="Envoyer", command=self.send).grid(row=2, column=1)
        tk.Button(master, text="RAZ", command=self.RAZ).grid(row=3, column=1)

    def createRectangles(self):
        for ligne in range(8):
            for colonne in range(8):
                self.squares.append(
                    self.canvas.create_rectangle(0 + 50*colonne,
                                                 0 + ligne*50,
                                                 50 + 50*colonne,
                                                 50 + ligne*50,
                                                 fill="white"))
                self.squaresState.append(False)

    def events(self, event):
        row = int(event.y / 50)
        col = int(event.x / 50)
        number = 8 * row + col
        if self.squaresState[number] == False:
            self.canvas.itemconfig(self.squares[number], fill="black")
            self.squaresState[number] = True
        else:
            self.canvas.itemconfig(self.squares[number], fill="white")
            self.squaresState[number] = False

    def fillWithAList(self, list):
        for i in range(64):
            if list[i] == False:
                self.canvas.itemconfig(self.squares[i], fill="white")
                self.squaresState[i] = False
            else:
                self.canvas.itemconfig(self.squares[i], fill="black")
                self.squaresState[i] = True

    def RAZ(self):
        list = []
        for i in range(64):
            list.append(False)
        self.fillWithAList(list)

    def open(self):
        path = filedialog.askopenfilename()
        if path == "":
            return
        f = open(path, 'r')
        data = f.readline()

        list = []
        for i in range(64):
            if data[i] == "0":
                list.append(False)
            else:
                list.append(True)
        self.fillWithAList(list)

    def send(self):

        string = ""
        compteur = 0
        output = []

        for i in range(8):
            output.append('')
        
        for element in self.squaresState:
            if element is False:
                string += "0"
            else:
                string += "1"

        for i in range(len(string)//8):
            while len(output[i]) < 8:
                output[i] += string[0]
                string = string[1:]

        for i in range(len(output)):
            output[i] = chr(65+i) + str(int(output[i], 2))
            arduino.write((output[i]).encode("utf-8"))


    def save(self):
        path = filedialog.asksaveasfilename()
        if path == "":
            return
        f = open(path, 'w')
        str = ""
        for element in self.squaresState:
            if element is False:
                str += "0"
            else:
                str += "1"

        f.write(str + '\n')


root = tk.Tk()
window = window(master=root)
window.mainloop()
